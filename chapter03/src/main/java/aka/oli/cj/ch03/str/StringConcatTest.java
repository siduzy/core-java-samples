package aka.oli.cj.ch03.str;

/**
 * 字符串拼接例子
 */
public class StringConcatTest {

    private static String s1 = "abc";
    private static String s2 = "def";


    /**
     * 字符串拼接， 使用 +
     */
    public static void concatWithPlus() {
        String result = s1 + s2;
        System.out.println(String.format("Method: %s, Result: %s", "concatWithPlus", result));
    }

    /**
     * 使用 {@link String#join(CharSequence, CharSequence...) } 来拼接字符串
     */
    public static void concatWithJoin() {
        String result = String.join("",s1, s2);
        System.out.println(String.format("Method: %s, Delimiter:%s, Result: %s", "concatWithJoin", "\"\"", result));
        result = String.join(",", s1, s2);
        System.out.println(String.format("Method: %s, Delimiter:%s, Result: %s", "concatWithJoin", "\",\"", result));
    }

    /**
     * 使用 {@link StringBuffer } 来拼接字符串
     */
    public static void concatWithBuffer() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(s1);
        buffer.append(s2);
        String result = buffer.toString();
        System.out.println(String.format("Method: %s, Result: %s", "concatWithBuffer", result));
    }

    /**
     * 使用 {@link StringBuilder } 来拼接字符串
     */
    public static void concatWithBuilder() {
        StringBuilder builder = new StringBuilder();
        builder.append(s1);
        builder.append(s2);
        String result = builder.toString();
        System.out.println(String.format("Method: %s, Result: %s", "concatWithBuilder", result));
    }

    public static void main(String[] args) {
        System.out.println("---------------");
        concatWithPlus();
        System.out.println("---------------");
        concatWithJoin();
        System.out.println("---------------");
        concatWithBuffer();
        System.out.println("---------------");
        concatWithBuilder();
    }
}
