package aka.oli.cj.ch03.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Read from a properties file and then write to another file
 */
public class FileReadWriteSample {


    /**
     * 从一个给定 uri 中读取一个 properties 文件内容
     *
     * @param uri properties 文件的位置
     * @return 读取到的 key-value 值
     * @throws IOException 如果文件打开时出现异常, 则抛出
     */
    public static Map<String, String> readProperties(URI uri) throws IOException {
        Scanner reader = new Scanner(Paths.get(uri), "UTF-8");
        try {
            String line = null;
            Map<String, String> kvs = new LinkedHashMap<>();
            while (reader.hasNextLine()) {
                line = reader.nextLine();
                int splitPos = line.indexOf("=");
                if (splitPos != -1) {
                    String key = line.substring(0, splitPos).trim();
                    StringBuffer valBuffer = new StringBuffer(line.substring(splitPos + 1).trim());
                    while (line.endsWith("\\")) {
                        line = reader.nextLine();
                        valBuffer.deleteCharAt(valBuffer.length() - 1);
                        valBuffer.append(line);
                    }
                    System.out.println(String.format("Read Key: %s, Value: %s", key, valBuffer));
                    kvs.put(key, valBuffer.toString());
                }
            }
            return kvs;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

    }

    /**
     * 将 key-value 值写到文件里
     *
     * @param kvs        key-value 对
     * @param outputFile 输出位置
     * @throws IOException 文件不存在或者编码错误时抛出
     */
    private static void writeProperties(Map<String, String> kvs, String outputFile) throws IOException {
        PrintWriter writer = new PrintWriter(outputFile, "UTF-8");
        try {
            for (String key : kvs.keySet()) {
                writer.println(String.format("%s=%s", key, kvs.get(key)));
            }
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    public static void main(String[] args) throws URISyntaxException, IOException {
        Map<String, String> kvs = readProperties(Thread.currentThread().getContextClassLoader().getResource("test.properties").toURI());
        writeProperties(kvs, "C:\\Users\\szhang1\\Desktop\\a.properties");
    }
}
