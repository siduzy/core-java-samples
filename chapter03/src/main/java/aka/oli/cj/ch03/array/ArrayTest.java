package aka.oli.cj.ch03.array;

import java.util.Arrays;

/**
 * 数组的常用使用方法
 */
public class ArrayTest {

    public static void sort(int[] ary) {
        Arrays.sort(ary);
    }

    public static int[] distinct(int[] ary) {
        int[] ret = Arrays.stream(ary).distinct().toArray();
        return ret;
    }

    public static void main(String[] args) {
        if (args.length > 0) {
            System.out.println(Arrays.toString(args));
        }
        System.out.println("---------------- Traversing ----------------------");
        int[] ary0 = new int[10];
        for (int x : ary0) {
            System.out.println(x);
        }

        System.out.println("---------------- Initialize ----------------------");
        int[] ary1 = new int[]{1, 3, 5, 7, 9, 8, 6, 4, 2, 1, 2, 3, 4, 5};
        System.out.println(Arrays.toString(ary1));

        System.out.println("----------------- Copy ---------------------");
        int[] ary1Copy = Arrays.copyOf(ary1, ary1.length);
        System.out.println(Arrays.toString(ary1Copy));

        System.out.println("----------------- Sort ---------------------");
        sort(ary1);
        System.out.println(Arrays.toString(ary1));

        System.out.println("--------------- Distinct -------------------");
        int[] ret = distinct(ary1);
        System.out.println(Arrays.toString(ret));
        System.out.println(Arrays.toString(ary1));

        System.out.println("---------------- Search --------------------");
        System.out.println(Arrays.toString(ary1));
        System.out.println(Arrays.binarySearch(ary1, 6));
    }
}
