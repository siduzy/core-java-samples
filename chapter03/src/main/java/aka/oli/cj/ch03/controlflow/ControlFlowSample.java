package aka.oli.cj.ch03.controlflow;

import java.util.Random;
import java.util.Scanner;

/**
 * 控制流例子
 */
public class ControlFlowSample {

    /**
     * 根据年龄段打印称谓.
     *
     * @param age
     */
    public static void ifElseTest(int age) {

        System.out.println(String.format("您的年龄是: %d 岁.", age));

        if (age > 0 && age <= 3) {
            System.out.println("您还是一个幼儿!");
        } else if (age > 3 && age <= 6) {
            System.out.println("您还是一个儿童!");
        } else if (age > 6 && age <= 17) {
            System.out.println("您还是一个少年!");
        } else if (age > 17 && age <= 40) {
            System.out.println("您是一个小青年!");
        } else if (age > 40 && age <= 50) {
            System.out.println("您正壮年!");
        } else if (age > 50 && age <= 65) {
            System.out.println("您已经步入中年!");
        } else {
            System.out.println("您是老年人了!");
        }
    }

    /**
     * 猜数字
     *
     * @param n 要猜的数字
     */
    public static void whileTest(int n) {
        try (Scanner in = new Scanner(System.in)) {
            int input = 0;
            while (true) {
                System.out.println("猜猜数字是多少(数字在 0 到 10 之间)?");
                input = in.nextInt();
                if (input > n) {
                    System.out.println("大了!");
                    continue;
                } else if (input < n) {
                    System.out.println("小了!");
                    continue;
                } else {
                    System.out.println(String.format("猜对了, 这个数是: %d", n));
                    break;
                }
            }
            System.out.println("欢迎下次再玩!");
        }
    }

    /**
     * switch 测试
     */
    public static void switchTest() {
        System.out.println("请输入您所使用的语言: 普通话请按 1, for English service, please press 2");
        try (Scanner in = new Scanner(System.in)) {
            int n = in.nextInt();
            switch (n) {
                case 1:
                    System.out.println("你好!");
                    break;
                case 2:
                    System.out.println("Hello!");
                    break;
                default:
                    System.out.println("按键有误! 再见!");
                    break;
            }
        }
    }

    public static void main(String[] args) {
//        ifElseTest(new Random().nextInt(100));
//        whileTest(new Random().nextInt(10));
        switchTest();
    }
}
