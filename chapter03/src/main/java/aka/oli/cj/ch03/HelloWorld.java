package aka.oli.cj.ch03;

/**
 * Hello world 示例
 */
public class HelloWorld {

    // 类常量
    private static final String DEFAULT_MESSAGE = "Hello world!";

    /**
     * Print message to screen.
     * @param message the message to print
     */
    public static void hello(String message) {
        System.out.println(message);
    }

    public static void main(String[] args) {
        hello(DEFAULT_MESSAGE);
    }
}
