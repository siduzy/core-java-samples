package aka.oli.cj.ch03;

import java.math.BigDecimal;

/**
 * 变量赋值例子
 */
public class TypeTest {

    public static void main(String[] args) {
        short s = 0;
        int i = 1000000000;
        long l = 10000000000l;
        float f = 3.14159f;
        double d = 0.9;
        double d2 = 2.0 - 1.1;
        System.out.println(d);
        System.out.println(d2);

        BigDecimal dcm = new BigDecimal(0.9);
        System.out.println(dcm.toString());

        BigDecimal dcm2 = new BigDecimal("0.9");
        System.out.println(dcm2.toString());
    }
}
