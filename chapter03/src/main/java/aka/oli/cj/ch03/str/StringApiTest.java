package aka.oli.cj.ch03.str;

import java.util.Arrays;

/**
 * 测试 {@link String} 类的常用 API
 */
public class StringApiTest {
    public static void main(String[] args) {
        String initial = " abcdefg_abcdefg ";

        System.out.println("----------------------------------------");
        System.out.println(initial.length()); // 得到字符串的长度

        System.out.println("----------------------------------------");
        String trimed = initial.trim(); // 去掉前后空格

        System.out.println("----------------------------------------");
        String[] strs = trimed.split("_"); // 按 _ 分割， 结果放到数组里
        System.out.println(Arrays.toString(strs));

        System.out.println("----------------------------------------");
        String joined = String.join("", strs); // 将数组合并， 分割符为空("")
        System.out.println(joined);

        System.out.println("----------------------------------------");
        System.out.println(joined.substring(7)); // 输出 _ 之后的 abcdefg
        System.out.println(joined.substring(0, 7)); // 输出 _ 之前的 abcdefg


        System.out.println("----------------------------------------");
        System.out.println(joined.startsWith("abcdefg")); // 是否是以 abcdefg 开始
        System.out.println(joined.endsWith("abcdefg")); // 是否是以 abcdefg 结束

        System.out.println("----------------------------------------");
        System.out.println(joined.indexOf("abc")); // 找出首次出现 "abc" 的位置
        System.out.println(joined.lastIndexOf("abc")); // 找出最后一次出现 "abc" 的位置

        System.out.println("----------------------------------------");
        System.out.println(joined.repeat(3)); // 重复三次

        System.out.println("----------------------------------------");
        System.out.println(joined.compareTo("ADC")); // 字典顺序比较
        System.out.println(joined.compareToIgnoreCase("ADC")); // 字典顺序比较, 不区分大小写

        System.out.println("----------------------------------------");
        String upperCased = joined.toUpperCase();
        System.out.println(upperCased); // 转成大写字母

        String lowerCased = upperCased.toLowerCase();
        System.out.println(lowerCased); // 转成小写字母

        System.out.println("----------------------------------------");
        System.out.println(joined.replace("abcdefg", "ha")); // 字符串替换

        System.out.println("----------------------------------------");
        System.out.println(joined.equals("ABCDEFGABCDEFG"));
        System.out.println(joined.equalsIgnoreCase("ABCDEFGABCDEFG"));
    }
}
