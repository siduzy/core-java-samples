package aka.oli.cj.ch03.bignumber;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * BigDecimal 演示
 */
public class BigDecimalTest {
    public static void main(String[] args) {
        BigDecimal val = BigDecimal.valueOf(2.0-1.9);
        System.out.println(val);

        val = new BigDecimal("2.0").subtract(new BigDecimal("1.9"));
        System.out.println(val);
        System.out.println("-------------------------------------------");
        try {
            val = new BigDecimal("20").divide(new BigDecimal("3"));
            System.out.println(val);
        } catch (Exception ex) {
            System.out.println("20 不能被 3 整除, 需要四舍五入.");
        }

        System.out.println("-------------------------------------------");
        val = new BigDecimal("20").divide(new BigDecimal("3"), 2, RoundingMode.UP);
        System.out.println(val);
        val = new BigDecimal("-20").divide(new BigDecimal("3"), 2, RoundingMode.UP);
        System.out.println(val);
        System.out.println("-------------------------------------------");
        val = new BigDecimal("20").divide(new BigDecimal("3"), 2, RoundingMode.DOWN);
        System.out.println(val);
        val = new BigDecimal("-20").divide(new BigDecimal("3"), 2, RoundingMode.DOWN);
        System.out.println(val);
        System.out.println("-------------------------------------------");
        val = new BigDecimal("20").divide(new BigDecimal("3"), 2, RoundingMode.CEILING);
        System.out.println(val);
        val = new BigDecimal("-20").divide(new BigDecimal("3"), 2, RoundingMode.CEILING);
        System.out.println(val);
        System.out.println("-------------------------------------------");
        val = new BigDecimal("20").divide(new BigDecimal("3"), 2, RoundingMode.FLOOR);
        System.out.println(val);
        val = new BigDecimal("-20").divide(new BigDecimal("3"), 2, RoundingMode.FLOOR);
        System.out.println(val);
        System.out.println("===========================================");

        // HALF_UP, 四舍五入
        val = new BigDecimal("25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_UP);
        System.out.println(val);
        val = new BigDecimal("-25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_UP);
        System.out.println(val);
        System.out.println("-------------------------------------------");
        // HALF_DOWN, 五舍六入
        val = new BigDecimal("25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_DOWN);
        System.out.println(val);
        val = new BigDecimal("-25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_DOWN);
        System.out.println(val);
        val = new BigDecimal("20").divide(new BigDecimal("3"), 1, RoundingMode.HALF_DOWN);
        System.out.println(val);
        val = new BigDecimal("-20").divide(new BigDecimal("3"), 1, RoundingMode.HALF_DOWN);
        System.out.println(val);
        System.out.println("-------------------------------------------");
        // 银行家舎入, 若精确位数值为奇数, 则行为和 HALF_UP 相同, 否则和 HALF_DOWN 相同
        val = new BigDecimal("25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN); // 1.25, .2 为偶数, 行为和 HALF_DOWN相同
        System.out.println(val);
        val = new BigDecimal("-25").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN);
        System.out.println(val);

        val = new BigDecimal("35").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN); // 1.75, .7 为奇数, 行为和 HALF_UP 相同
        System.out.println(val);
        val = new BigDecimal("-35").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN);
        System.out.println(val);

        val = new BigDecimal("45").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN); // 2.25, .2 为奇数, 行为和 HALF_DOWN 相同
        System.out.println(val);
        val = new BigDecimal("-45").divide(new BigDecimal("20"), 1, RoundingMode.HALF_EVEN);
        System.out.println(val);
    }
}
