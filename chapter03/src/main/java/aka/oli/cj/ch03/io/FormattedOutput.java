package aka.oli.cj.ch03.io;

/**
 * 常用格式化输出
 */
public class FormattedOutput {

    private static void formatFloat() {
        System.out.println(String.format("%f", Math.PI));   // f 代表浮点数输出, 默认输出6位小数输出
        System.out.println(String.format("%+f", Math.PI)); // +f 代表浮点数输出, 默认输出6位小数输出, 带正负号
        System.out.println(String.format("%.2f", Math.PI)); // .2f 代表小数点后有两位
        System.out.println(String.format("%8.2f", Math.PI)); // 8.2f 代表小数点后有两位, 总位数为8位, 不够则前面补空格, 超过正常输出
        System.out.println(String.format("%08.2f", Math.PI)); // 08.2f 代表小数点后有两位, 总位数为8位, 不够则前面补0, 超过正常输出
        System.out.println(String.format("%,08.2f", Math.PI)); // ,08.2f 代表小数点后有两位, 总位数为8位, 不够则前面补0, 超过正常输出, 且整数部分按','分割法分割
    }

    private static void formatInteger() {
        final int NUMBER = 10000000;
        System.out.println(String.format("%d", NUMBER)); // d 代表整数输出
        System.out.println(String.format("%+d", NUMBER)); // +d 代表整数输出, 带正负号
        System.out.println(String.format("%12d", NUMBER)); // 12d 代表, 总位数为10位, 不够则前面补空格, 超过正常输出
        System.out.println(String.format("%012d", NUMBER)); // 012d 代表, 总位数为10位, 不够则前面补0, 超过正常输出
        System.out.println(String.format("%,012d", NUMBER)); // ,12d 代表, 总位数为10位, 不够则前面补0, 超过正常输出, 且按','分割法分割
    }

    private static void formatHex() {
        final int NUMBER = 257;
        System.out.println(String.format("%x", NUMBER)); // x代表十六进制输出
        System.out.println(String.format("%#x", NUMBER)); // #x代表十六进制输出, 且附带'0x'前缀
    }

    private static void formatString() {
        System.out.println(String.format("你好 %s!", "Sid"));
    }

    private static void formatOther() {
        System.out.println(String.format("输出百分号: %%20")); // %% 用于输出百分号自己
        System.out.println(String.format("输出不区分平台的换行: %n")); // %n 用来输出跨平台的空行.(windows 是 \r\n. linux 是 \n, mac 是 \r)
        System.out.println("(上面会有一空行)");

    }

    public static void main(String[] args) {
        System.out.println("-----------------------------");
        formatString();
        System.out.println("-----------------------------");
        formatFloat();
        System.out.println("-----------------------------");
        formatInteger();
        System.out.println("-----------------------------");
        formatHex();
        System.out.println("-----------------------------");
        formatOther();
    }

}
