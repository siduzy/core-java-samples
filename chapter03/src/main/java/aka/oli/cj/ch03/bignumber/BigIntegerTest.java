package aka.oli.cj.ch03.bignumber;

import java.math.BigInteger;

/**
 * BigInteger 类的基本操作
 */
public class BigIntegerTest {
    public static void main(String[] args) {
        BigInteger bigInt1 = new BigInteger("100000000000000000000000000000000000000000000000");
        BigInteger bigInt2 = new BigInteger("99999999999999999999999999999999999999999999999");
        BigInteger subtractRes = bigInt1.subtract(bigInt2);
        System.out.println(subtractRes.equals(BigInteger.ONE));

        BigInteger addRes = bigInt2.add(new BigInteger("-1"));
        System.out.println(addRes.equals(bigInt1));

        BigInteger multiplyRes = bigInt1.multiply(bigInt2);
        System.out.println(multiplyRes.toString());

        BigInteger divideRes = bigInt1.divide(bigInt2);
        System.out.println(divideRes.toString());

        BigInteger modRes = bigInt1.mod(bigInt2);
        System.out.println(modRes.toString());
    }
}
