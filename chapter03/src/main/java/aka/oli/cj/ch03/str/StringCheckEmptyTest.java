package aka.oli.cj.ch03.str;

/**
 * 检测字符串的 null， 空 和 空白
 */
public class StringCheckEmptyTest {


    /**
     * 检测字符串是否为 null
     *
     * @param str 要检查的字符串
     * @return {@code true} 如果字符串为 null， 否则返回 {@code false}
     */
    public static boolean isNull(String str) {
        return str == null;
    }

    /**
     * 检测字符串是否为 空
     *
     * @param str 要检查的字符串
     * @return {@code true} 如果字符串为空， 否则返回 {@code false}
     */
    public static boolean isEmpty(String str) {
        return isNull(str) || str.isEmpty();
    }

    /**
     * 检查字符串是否为 空串， 空串是只包含 ' ' 或者 '\t' 或者 '\n'的字符串
     *
     * @param str 要检查的字符串
     * @return {@code true} 如果字符串为空串， 否则返回 {@code false}
     */
    public static boolean isBlank(String str) {
        return isNull(str) || isEmpty(str.trim());
    }

    public static void main(String[] args) {
        System.out.println(isNull(null));
        System.out.println(isEmpty(""));
        System.out.println(isBlank("\t\n \t\n"));
        System.out.println(isNull("abc"));
        System.out.println(isEmpty("abc"));
        System.out.println(isBlank("abc"));
    }
}
