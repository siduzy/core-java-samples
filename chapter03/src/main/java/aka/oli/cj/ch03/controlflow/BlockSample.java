package aka.oli.cj.ch03.controlflow;

/**
 * 代码块例子
 */
public class BlockSample {

    private static int i = 1;

    // 类代码块, 在'出现类名'的时候执行;
    static {
        System.out.println(String.format("静态代码块中执行: %d", ++i));
    }

    public BlockSample() {
        System.out.println(String.format("构造函数中执行: %d", ++i));
    }

    // 初始化代码块, 在创建实例后执行
    {
        System.out.println(String.format("初始化代码块中执行: %d", ++i));
    }

    /**
     * 块作用域
     */
    public static void nestedBlock() {

        int i = 10;
        {
            i = 100;
            System.out.println(String.format("普通代码块中执行被覆盖值: %d", ++i));
        }
    }

    public static void runRaw() {
        nestedBlock();
    }

    public static void runInstance() {
        new BlockSample();
        BlockSample.nestedBlock();
    }

    public static void main(String[] args) {
        System.out.println("--------------------------");
        runRaw();
        System.out.println("--------------------------");
        runInstance();
        System.out.println("--------------------------");
        runInstance();
    }
}
