package aka.oli.cj.ch03.io;

import java.util.Scanner;


/**
 * 从标准输入(键盘)来读取字符串
 */
public class StdInputReader {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("你叫啥?");
        String name = in.nextLine();

        System.out.println("你多大?");
        int age = in.nextInt();

        System.out.println("验证问题: 2.0 - 1.1 = ?");
        double answer = in.nextDouble();

        if (Double.compare(answer, 0.9) != 0) {
            System.out.println("回答错误! 再见!");
            System.exit(-1);
        }

        System.out.println(String.format("你好! %s, 明年你 %d 了!", name, age+1));
    }
}
